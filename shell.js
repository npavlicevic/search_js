var Class = function() {};
Class.prototype.sort = function(arr, helper, gaps) {
  // use shell sort
  // like insert sort with gaps
  // worst o(n^2)
  // best o(n log^2 n)
  if(gaps == undefined) {
    gaps = [128, 64, 32, 16, 8, 4, 2, 1];
  }
  for(var i = 0; i < gaps.length; i++) {
    var gap = gaps[i];
    for(var j = gap; j < arr.length; j++) {
      var temp = arr[j];
      for(var k = j; k >= gap && arr[k - gap] > arr[k]; k -= gap) {
        helper.swap(arr, k - gap, k);
      }
      arr[k] = temp;
    }
  }
};
module.exports = Class;
