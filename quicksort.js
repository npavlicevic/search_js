var Class = function() {};
Class.prototype.split = function(arr, helper, lo, hi) {
  var pivot = Math.floor((lo + hi) / 2);
  var pivotValue = arr[pivot];
  helper.swap(arr, pivot, hi);
  var i = lo, storeIndex = lo;
  for(; i < hi; i++) {
    if(arr[i] <= pivotValue) {
      // put larger instead of smaller
      // follow while smaller
      helper.swap(arr, storeIndex, i);
      storeIndex++;
    }
  }
  arr[storeIndex] = pivotValue;
  helper.swap(arr, storeIndex, hi);
  return storeIndex;
};
Class.prototype.sort = function(arr, helper, lo, hi) {
  // sort using quick sort
  // recursive
  // uses inserts kind of
  // average o(n log n)
  if(lo < hi) {
    var pivot = this.split(arr, helper, lo, hi);
    this.sort(arr, helper, lo, pivot - 1);
    this.sort(arr, helper, pivot + 1, hi);
  }
}
module.exports = Class;
