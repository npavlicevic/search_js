var Class = function() {};
Class.prototype.sort = function(arr, helper) {
  // sort using combsort
  // bubble sort with gaps
  var gap = arr.length, swaps = 1, i, j;
  for(; (gap >= 1) || (swaps === 1);) {
    gap = Math.floor(gap / 1.2);
    swaps = 0;
    for(i = 0, j = gap; j < arr.length; i++, j++) {
      if(arr[i] > arr[j]) {
        helper.swap(arr, i, j);
        swaps = 1;
      }
    }
  }
};
module.exports = Class;
