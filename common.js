var Class = function() {};
Class.prototype.random = function(arr, size, max) {
  // fill arr with random values
  for(i = 0; i < size; i++) {
    arr[i] = Math.floor(Math.random() * max);
  }
};
Class.prototype.swap = function(arr, a, b) {
  // swap elements in array
  var temp = arr[a];
  arr[a] = arr[b];
  arr[b] = temp;
}
Class.prototype.printArr = function(arr) {
  // print array
  for(i = 0; i < arr.length; i++) {
    console.log(arr[i] + " ");
  }
  console.log("\n");
}
module.exports = Class;
