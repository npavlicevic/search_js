var Class = function() {};
Class.prototype.sort = function(arr, helper) {
  // bubble sort
  var swaps = 1;
  for(; swaps === 1;) {
    swaps = 0;
    for(i = 0, j = 1; j < arr.length; i++, j++) {
      if(arr[i] > arr[j]) {
        helper.swap(arr, i, j);
        swaps = 1;
      }
    }
  }
};
module.exports = Class;
