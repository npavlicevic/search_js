var Class = function() {};
Class.prototype.position = function(position, pattern) {
  // position of character in pattern
  var i = 0;
  for(i = 0; i < 128; i++) {
    position[i] = 0;
  }
  for(i = 0; i < pattern.length; i++) {
    position[pattern.charCodeAt(i)] = i;
  }
};
Class.prototype.search = function(text, pattern) {
  // looking glass heuristing
  // search from last character
  // if character not in pattern move past in text
  // thething
  // thin
  var position = [];
  this.position(position, pattern);
  var i = 0, j, size = text.length, _size = pattern.length;
  for(;i < (size - _size);) {
    j = _size - 1;
    for(;pattern[j] == text[i + j];) {
      j--;
      if(j < 0) {
        return i;
      }
    }
    if(j < position[text.charCodeAt(i + j)]) {
      console.log("j less", j, text[i + j], text.charCodeAt(i + j), position[text.charCodeAt(i + j)]);
      // bad character move forward in text
      i++;
    } else {
      // character not in pattern move past in text
      i = i + j - position[text.charCodeAt(i + j)];
    }
  }
  return -1;
};
module.exports = Class;
